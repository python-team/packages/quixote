From 5ca73f7e2de39374fad8b1a7a29d6ea9fd09f35c Mon Sep 17 00:00:00 2001
From: SVN-Git Migration <python-modules-team@lists.alioth.debian.org>
Date: Thu, 8 Oct 2015 13:40:07 -0700
Subject: 02_setup.py

Patch-Name: 02_setup.py
---
 setup.py | 3 ++-
 1 file changed, 2 insertions(+), 1 deletion(-)

diff --git a/setup.py b/setup.py
index c82fa26..36bf36d 100644
--- a/setup.py
+++ b/setup.py
@@ -10,6 +10,7 @@
 import sys
 import os
 from distutils import core
+from setuptools import setup
 from distutils.extension import Extension
 from quixote.ptl.qx_distutils import qx_build_py
 from quixote import __version__
@@ -71,4 +72,4 @@ if hasattr(core, 'setup_keywords'):
     if 'platforms' in core.setup_keywords:
         kw['platforms'] = 'Most'
 
-core.setup(**kw)
+setup(**kw)
